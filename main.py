#!/usr/bin/env python

# Run this with
# PYTHONPATH=. DJANGO_SETTINGS_MODULE=testsite.settings testsite/tornado_main.py
# Serves by default at
# http://localhost:8080/hello-tornado and
# http://localhost:8080/hello-django


from tornado.options import options, define, parse_command_line
from DecoLed.wsgi import application
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
from DecoLed.settings import STATIC_ROOT
import tornado.websocket
from tornado.websocket import WebSocketClosedError

from django.dispatch import receiver
from django.conf import settings
from django.utils.module_loading import import_module
import django.contrib.auth
from api.views import order_created
from api.serializers import order_status_changed
from api.models import Order


@receiver(order_created)
def notify_staff(sender, **kwargs):
    AdminSocketHandler.notify_new_order(kwargs['instance'])


@receiver(order_status_changed)
def notify_user(sender, **kwargs):
    print('An order has its status changed')
    ClientSocketHandler.notify_order_status_changed(kwargs['instance'])

define('port', type=int, default=8000)


class AdminSocketHandler(tornado.websocket.WebSocketHandler):
    staff = set()

    def check_origin(self, origin):
        return True

    @classmethod
    def notify_new_order(cls, order):
        print('Notificando staff')
        # data = {'instance': order, 'render': cls.render_string("order_template.html", order=order)}
        for handler in cls.staff:
            # TODO: ENVIAR CSFR TOKEN
            rows = [row for row in order.rows.all()]
            data = {
				'render': handler.render_string("order_template.html", order=order, rows=order.rows.all()).decode(),
				'count': Order.objects.count()
			}
            try:
                handler.write_message(data)
            except WebSocketClosedError:
                print('No se pudo notificar a un staff')

    def get_current_user(self, session_key=None):
        if(not session_key):
            return None
        engine = import_module(settings.SESSION_ENGINE)
        # print(engine)

        class Dummy(object):
            pass

        django_request = Dummy()
        django_request.session = engine.SessionStore(session_key)
        user = django.contrib.auth.get_user(django_request)
        return user

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self, *args, **kwargs):
        session_cookie = self.request.cookies['sessionid'].value
        user = self.get_current_user(session_cookie)
        if(user.is_staff):
            AdminSocketHandler.staff.add(self)
            print('{} se ha conectado al panel de pedidos.'.format(user.username))
            try:
                self.write_message({'count': Order.objects.count()})
            except WebSocketClosedError:
                print('Error al conectarse al panel de pedidos.')
        else:
            self.close(403)

    def on_message(self, message):
        print('Got message {}'.format(message))
        if(message == 'clean'):
            print('Limpiando pedidos huerfanos')
            Order.objects.all().exclude(status='pending').delete()
            try:
                self.write_message({'count': Order.objects.count()})
            except WebSocketClosedError:
                print('Error al conectarse al panel de pedidos.')
        
    def on_close(self):
        if(self in AdminSocketHandler.staff):
            AdminSocketHandler.staff.remove(self)


class ClientSocketHandler(tornado.websocket.WebSocketHandler):
    clients = {}

    def check_origin(self, origin):
        return True

    @classmethod
    def notify_order_status_changed(cls, order):
        if(order.user.username in ClientSocketHandler.clients.keys()):
            handler = ClientSocketHandler.clients[order.user.username]
            data = {'render': handler.render_string("status_template.html", order=order, status=order.status).decode(),
                    'status': order.status}
            try:
                handler.write_message(data)
                print('El cliente {} ha sido notificado.'.format(order.user.first_name))
                order.user = None
                order.save()
            except WebSocketClosedError:
                print('No se pudo notificar al cliente.')

    def get_current_user(self, session_key=None):
        if(not session_key):
            return None
        engine = import_module(settings.SESSION_ENGINE)
        # print(engine)

        class Dummy(object):
            pass

        django_request = Dummy()
        django_request.session = engine.SessionStore(session_key)
        user = django.contrib.auth.get_user(django_request)
        return user

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self, *args, **kwargs):
        session_cookie = self.request.cookies['sessionid'].value
        user = self.get_current_user(session_cookie)
        if(user.order):
            if(user.order.status != 'pending'):
                data = {
                    'render': self.render_string(
                        "status_template.html", order=user.order, status=user.order.status).decode(),
                    'status': user.order.status}
                try:
                    self.write_message(data)
                    user.order.user = None
                    user.order.save()
                    print('El cliente {} ha sido notificado.'.format(user.first_name))
                except WebSocketClosedError:
                    print('No se pudo notificar al cliente.')
            else:
                ClientSocketHandler.clients[user.username] = self
                print('{} se ha conectad a la cola de espera.'.format(user.first_name))
        else:
            self.close(403)

    def on_close(self):
        session_cookie = self.request.cookies['sessionid'].value
        user = self.get_current_user(session_cookie)
        if(user):
            if(user.username in ClientSocketHandler.clients.keys()):
                ClientSocketHandler.clients.pop(user.username)


class HelloHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Hello from tornado')


def main():
    parse_command_line()
    wsgi_app = tornado.wsgi.WSGIContainer(application)
    tornado_app = tornado.web.Application(
        [
            ('/hello-tornado', HelloHandler),
            ('/static/(.*)', tornado.web.StaticFileHandler, {'path': STATIC_ROOT}),
            ('/order_status', ClientSocketHandler),
            ('/admin_status', AdminSocketHandler),
            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ], template_path="usuarios/templates")
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
