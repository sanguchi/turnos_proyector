CREATE DATABASE django_decoled_db;
CREATE USER django_decoled_user WITH PASSWORD 'django_decoled_password';
ALTER ROLE django_decoled_user SET client_encoding TO 'utf8';
ALTER ROLE django_decoled_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE django_decoled_user SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE django_decoled_db TO django_decoled_user;