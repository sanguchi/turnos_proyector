console.log('TEST');
var order = [];

function updateOrder(drink_id, count){
    for(var i = 0; i < order.length; i++){
        var row = order[i];
        if(drink_id == row.drink) {
            if(count == 0){
                order.pop(i);
                return;
            }
            order[i].cantity = count;
            return
        }
    }
    order.push({drink:drink_id, cantity: count});
}

$('.btn-up').on('click', function(e) {
    console.log("Up");
    var counter_element = $(this).closest('.row').children().first();
    // console.log(counter_element);
    var drink_id = parseInt(counter_element.attr('id').split('_')[2]);
    var counter = parseInt(counter_element.html());
    if(counter < 3) {
        counter++;
        updateOrder(drink_id, counter);
        counter_element.html(counter);
    }
    // console.log(counter, drink_id);
});

$('.btn-down').on('click', function(e) {
    console.log("Down");
    var counter_element = $(this).closest('.row').children().first();
    // console.log(counter_element);
    var drink_id = parseInt(counter_element.attr('id').split('_')[2]);
    var counter = parseInt(counter_element.html());
    if(counter > 0) {
        counter--;
        updateOrder(drink_id, counter);
        counter_element.html(counter);
    }
    // console.log(counter, drink_id);
});

$('.btn-details').click( function(){
    $(this).find('i').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
});

$('.btn-ok').on('click', function(e) {
    var name = $(".form-control").val();
    console.log("Sending order");

    if(order.length){
        if(name) {
            var post_data = {name:name,rows:order, csrfmiddlewaretoken:$("input[name='csrfmiddlewaretoken']")[0].value};
            console.log(JSON.stringify(post_data), post_data);
            $(this).find('i').toggleClass('fa-check btn-success btn-primary fa-refresh fa-spin');
            $.ajax({
                type: 'POST',
                url: $("meta[property='api-url']").attr('content'),
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(post_data)
                })
                .done(function(data){
                    var message = 'Pedido realizado con exito, por favor no cierre la pagina<br>Recibira una notificacion cuando su pedido este listo.';
                    //$('.modal-p').html('Pedido realizado con exito, por favor no cierre la pagina<br>Recibira una notificacion cuando su pedido este listo.');
                    //$("#myModal").modal();
                    bootbox.alert({message: message,
                        buttons: {
                            ok: {
                                label: '<i class="glyphicon glyphicon-ok"></i>',
                                className: 'btn-success btn-circle btn-done'},},
                        callback: function (result) {
                            location.reload();}});
                })
                .fail(function(data){
                    var message = 'Error del servidor: <br>' + data.responseText;
                    // $('.modal-p').html('Error del servidor: <br>' + data.responseText);
                    // console.log(data);
                    // $("#myModal").modal();
                    bootbox.alert({message: message, callback: function (result) {location.reload();}});
                });
            $(this).find('i').toggleClass('fa-check btn-success btn-primary fa-refresh fa-spin');  
        }
        else {
            var message = 'Por favor introduzca su nombre';
            bootbox.alert({message:message, size:'small'});
            // $('.modal-p').html('Por favor introduzca su nombre');
            // $("#myModal").modal();
        }
    }
    else {
        var message = 'Por favor especifique su pedido.';
        // $('.modal-p').html('Por favor especifique su pedido.');
        // $("#myModal").modal();
        bootbox.alert({message:message, size:'small'});
    }
});

$('.btn-reload').on('click', function(e) {
 location.reload();
});
var reload_timer;

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/order_status";
        updater.socket = new WebSocket(url);
        updater.socket.onopen = function(event) {
            $('#btn-refresh')
            .removeClass('btn-primary btn-danger')
            .addClass('btn-success')
            .find('.fa-refresh').first().addClass('fa-spin');
        };
        updater.socket.onclose = function(event) {
            $('#btn-refresh')
            .removeClass('btn-primary btn-success')
            .addClass('btn-danger')
            .find('.fa-refresh').first().removeClass('fa-spin');
            updater.start();
        };
        updater.socket.onerror = function(event) {
            console.log('errror xfdd');
        };
        updater.socket.onmessage = function(event) {
            updater.showMessage(event.data);
        };
    },

    showMessage: function(message) {
        var orderDOM = JSON.parse(message).render;
        console.log('New status received: ', JSON.parse(message).status);
        $('.panel').replaceWith(orderDOM);
    }
};

function getWebsocketStatus(event) {
    switch(updater.socket.readyState) {
        case 0:
            return 'Conectando al servidor';
        case 1:
            return 'Conectado en tiempo real';
        case 2:
            return 'Desconectandose';
        case 3:
            return 'Desconectado';
        default:
            return 'ERROR';
    }
}