var last_order;

// $('.btn-done').on('click', acceptOrderCallback);
// $('.btn-cancel').on('click', cancelOrderCallback);
$('.btn-drink-toggle').on('click', toggleDrinkAvailableCallback);

function acceptOrderCallback(button_object) {
    console.log('Button done clicked.');
    var done_btn = $(button_object);
    var row = done_btn.closest('.row').first();
    var order_id = row.attr('id').split('_')[2];
    setOrderStatus(order_id, 'done', 'Pedido aceptado').then( function() {
        row.find('.btn-cancel').first().off('click').prop('onclick', null).on('click', function(e){
            row.remove();
            $('#drink_details_' + order_id).remove();
        })
        .find('i').toggleClass('glyphicon-remove').toggleClass('glyphicon-trash');
        done_btn.remove();
    });
}

function cancelOrderCallback(button_object) {
    console.log('Button cancel clicked.');
    var cancel_btn = $(button_object);
    var row = cancel_btn.closest('.row').first();
    var order_id = row.attr('id').split('_')[2];

    setOrderStatus(order_id, 'cancelled', 'Pedido cancelado')
    .then( function(){
        //console.log('this: ', $(this));
        //console.log
        row.remove();
        $('#drink_details_' + order_id).remove();
    });
}

function toggleDrinkAvailableCallback(event) {
    console.log('Button toggle clicked.', $(this), $(this).attr('id'));
    var drink_id = $(this).attr('id');
    var available = $(this).hasClass('btn-success');
    toggleDrinkAvailable(drink_id, !available).done(function(data) {
        console.log("toggled, data: ", data);
        $('#' + drink_id).toggleClass('btn-success').toggleClass('btn-danger');
    });
}

function toggleDrinkAvailable(drink_id, available) {
    console.log('Changing available field for drink ', drink_id, ' to ', available);
    return $.ajax({
        type: 'PATCH',
        url: "/api/drinks/" + drink_id + "/",
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({'available': available})
        })
        .fail(function(data) {
            var error = data.responseText || data;
            console.log("ERROR: ", data);
            var message = "Se ha producido un error: <br>" + error;
            bootbox.alert(message);
        });
}

function setOrderStatus(order_id, status, done_message, callback) {
    return $.ajax({
        type: 'PATCH',
        url: "/api/orders/" + order_id + "/",
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({'status': status})
        })
    .done(function(data){
        bootbox.alert(done_message);
        console.log("data: ", data);
    })
    .fail(function(data){
        var error = data.responseText || data;
        console.log("data: ", data );
        var message = 'Error del servidor: <br>' + error;
        bootbox.alert(message);
    });
}
function updateRows() {

}

$(document).ready(function() {
    updater.start();
    $('[data-toggle="tooltip"]').tooltip({
        title: getWebsocketStatus,
        trigger: 'hover',
    });
    $('.clean_database').on('click', function(e){
        console.log('Cleaning orders');
        updater.socket.send('clean');
    });
});

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/admin_status";
        updater.socket = new WebSocket(url);
        updater.socket.onopen = function(event) {
            $('#btn-refresh')
            .removeClass('btn-primary btn-danger')
            .addClass('btn-success')
            .find('.fa-refresh').first().addClass('fa-spin');
        };
        updater.socket.onclose = function(event) {
            $('#btn-refresh')
            .removeClass('btn-primary btn-success')
            .addClass('btn-danger')
            .find('.fa-refresh').first().removeClass('fa-spin');
            console.log('Reconnecting');
            setTimeout(updater.start, 2000);
        };
        updater.socket.onmessage = function(event) {
            updater.showMessage(event.data);
        };
    },

    showMessage: function(message) {
        var parsedMessage = JSON.parse(message);
        console.log('Message :', message, ' - Parsed:', parsedMessage);
        if(parsedMessage.hasOwnProperty('render')) {
            $('.no-orders').remove();
            var orderDOM = parsedMessage.render;
            last_order = orderDOM;
            $('.inner.cover').append(orderDOM);
        }
        if(parsedMessage.hasOwnProperty('count')) {
            console.log('Actualizando contador');
            $('.counter_span').html(parsedMessage.count);
        }
        
        // console.log(message);
        // console.log(JSON.parse(message));
//        var existing = $("#m" + message.id);
//        if (existing.length > 0) return;
//        var node = $(message.html);
//        node.hide();
//        $("#inbox").append(node);
//        node.slideDown();
    }
};

function getWebsocketStatus(event) {
    switch(updater.socket.readyState) {
        case 0:
            return 'Conectando al servidor';
        case 1:
            return 'Conectado en tiempo real';
        case 2:
            return 'Desconectandose';
        case 3:
            return 'Desconectado';
        default:
            return 'ERROR';
    }
}
