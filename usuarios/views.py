from django.shortcuts import render, redirect, reverse
from .forms import LoginForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from lazysignup.decorators import allow_lazy_user, require_nonlazy_user
from api import models
from django.contrib.auth.decorators import user_passes_test


def do_login(request):
    if(request.method == 'POST'):
        form = LoginForm(request.POST)
        if(form.is_valid()):
            # print(form.cleaned_data)
            user = authenticate(request, **form.cleaned_data)
            if(user is not None):
                login(request, user)
                return redirect(reverse('usuarios:index'))
            else:
                messages.error(request, "Datos de ingreso invalidos.")
                return render(request, 'login.html', {'form': form})
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form})


def do_logout(request):
    logout(request)
    return redirect(reverse('usuarios:index'))


def index(request):
    drinks = models.Drink.objects.filter(available=True)
    # print("DRINKS, ", drinks)
    return render(request, 'home.html', {'drinks': drinks})


@allow_lazy_user
def new_index(request):
    drinks = models.Drink.objects.filter(available=True)
    # print("DRINKS, ", drinks)
    if(models.Order.objects.filter(user=request.user).count()):
        order = request.user.order
        if(order.status in ['cancelled', 'done']):
            order.user = None
            order.save()
        print('ORDER STATUS: ', order.status)
        return render(request, 'user_wait.html', {'order': order})

    return render(request, 'new_home.html', {'drinks': drinks})


@require_nonlazy_user('/login')
@user_passes_test(lambda u: u.is_staff, '/login')
def order_manager(request):
    orders = models.Order.objects.filter(status='pending').exclude(user=None)
    return render(request, 'order_manager.html', {
		'orders': orders,
		'count': models.Order.objects.count()
    })


@require_nonlazy_user('/login')
@user_passes_test(lambda u: u.is_staff, '/login')
def drink_manager(request):
    drinks = models.Drink.objects.all()
    return render(request, 'drink_manager.html', {'drinks': drinks})
