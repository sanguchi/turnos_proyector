from django.conf.urls import url
from .views import index
from . import views
app_name = 'usuarios'

urlpatterns = [
    # url(r'drinks/$', drink_manager, name='drink_manager'),
    url(r'old/^$', index, name='old_index'),
    url(r'^$', views.new_index, name='index'),
    url(r'login/$', views.do_login, name='login'),
    url(r'logout/$', views.do_logout, name='logout'),
    url(r'order_manager/$', views.order_manager, name='order_manager'),
    url(r'drink_manager/$', views.drink_manager, name='drink_manager'),
]
