from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from . import models
from lazysignup.utils import is_lazy_user
from django.contrib.auth.models import Group
from django.dispatch import Signal

order_status_changed = Signal(providing_args=['instance'])


class OrderRowSerializer(ModelSerializer):
    drink = serializers.PrimaryKeyRelatedField(
        queryset=models.Drink.objects.filter(available=True),
    )

    class Meta:
        model = models.OrderRow
        fields = ('drink', 'cantity')


class OrderSerializer(ModelSerializer):
    rows = OrderRowSerializer(many=True, read_only=False)
    user = serializers.StringRelatedField(read_only=True)
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Order
        fields = ('id', 'rows', 'user', 'status',)

    def update(self, instance, validated_data):
        print('data: ', validated_data)
        instance.status = validated_data['status']
        instance.save()
        order_status_changed.send(self.__class__, instance=instance)
        return instance

    def create(self, validated_data):
        rows = validated_data.pop('rows')
        user = self.context['request'].user
        if(is_lazy_user(user)):
            used_group, created = Group.objects.get_or_create(name="username_modified")
            if(created):
                used_group.save()
            if(not user.groups.filter(name="username_modified").count()):
                user.first_name = self.initial_data["name"] if "name" in self.initial_data else "Desconocido"
                user.groups.add(used_group)
                user.save()
        order = models.Order.objects.create(user=user)
        for row in rows:
            models.OrderRow.objects.create(order=order, **row)
        return order


class DrinkSerializer(ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Drink
        fields = ('id', 'name', 'description', 'available')
