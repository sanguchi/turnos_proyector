from django.contrib import admin
from .models import Drink, Order, OrderRow


# Register your models here.
admin.site.register(Drink)
admin.site.register(Order)
admin.site.register(OrderRow)
