from rest_framework import viewsets, status
from . import models
from .serializers import OrderSerializer, DrinkSerializer
from rest_framework.response import Response
from rest_framework import permissions
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication
from django.dispatch import Signal

order_created = Signal(providing_args=['instance'])


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class OrderViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = models.Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    # authentication_classes = (CsrfExemptSessionAuthentication,)

    def perform_create(self, serializer):
        print('Creating order for ', self.request.user)
        serializer.save(user=self.request.user)
        order_created.send(self.__class__, instance=self.request.user.order)

    @csrf_exempt
    def create(self, request, *args, **kwargs):
        if(hasattr(self.request.user, 'order')):
            print('User has an order pending')
            return Response({'error': 'Ya tienes un pedido en progreso.'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return super(OrderViewSet, self).create(request, *args, **kwargs)


class DrinkViewSet(viewsets.ModelViewSet):
    """
    List, create, edit, delete Drink instances.
    """
    queryset = models.Drink.objects.all()
    serializer_class = DrinkSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
