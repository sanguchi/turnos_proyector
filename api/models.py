from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Drink(models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=64, blank=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Order(models.Model):

    choices = [
        ("done", "Listo"),
        ("pending", "Pendiente"),
        ("cancelled", "Cancelado"),
    ]
    status = models.CharField(max_length=64, default="pending", choices=choices)
    user = models.OneToOneField(User, related_name='order', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        if(self.user):
            return "{}'s order.".format(self.user.username)
        else:
            return "Order: {}".format(
            ','.join(['{} {}'.format(
            row.cantity, row.drink.name) for row in self.rows.all()]))


class OrderRow(models.Model):
    order = models.ForeignKey(Order, related_name='rows', on_delete=models.CASCADE)
    drink = models.ForeignKey(Drink, limit_choices_to={'available': True})
    cantity = models.PositiveIntegerField(default=1)
