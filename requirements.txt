Django==1.11.4
django-lazysignup==1.1.2
djangorestframework==3.6.4
python-decouple==3.1
pytz==2017.2
six==1.11.0
tornado==4.5.2
psycopg2==2.7.3.2
